# Caso de estudio 1

El primer caso de estudio crear en un sitio web Ecommerce para una tienda de libros tipo Amazon. El nombre de esta tienda ficticia es Babook, y en ella se pueden hacer varias cosas, como por ejemplo:
1. Hacer compras
2. Administrar pedidos
3. Administrar productos (administrador)
4. Gestionar historial de pedidos
5. Comentar y calificar productos

El sistema est� hecho en Laravel, un framework de PHP. 

## Realizado para Seminario de Ingenier�a de Software I
## Profesor: Luis Antonio Medell�n Serna
## Alumno: Josu� Renaud Hern�ndez Guzm�n - 217294598

